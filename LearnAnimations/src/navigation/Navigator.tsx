import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { PointerScreen } from '@screens/PointerScreen';
import { HeartScreen } from '@screens/HeartScreen';
import { PlusScreen } from '@screens/PlusScreen';

import React from 'react';
import { fade, flipToScreen } from './transitions';

const Stack = createStackNavigator();
export const Navigator = () => {
  return (
    <NavigationContainer
      theme={{
        dark: true,
        colors: {
          primary: 'transparent',
          background: 'black',
          card: 'transparent',
          text: 'transparent',
          border: 'transparent',
          notification: 'transparent',
        },
      }}>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
          cardShadowEnabled: false,
          transitionSpec: {
            open: {
              animation: 'timing',
              config: {
                duration: 1000,
              },
            },
            close: {
              animation: 'timing',
              config: {
                duration: 1000,
              },
            },
          },
          animationEnabled: true,
          cardStyleInterpolator: flipToScreen,
        }}>
        <Stack.Screen name="Heart" component={HeartScreen} />
        <Stack.Screen
          name="Duck"
          component={PointerScreen}
          options={{ cardStyleInterpolator: fade }}
        />
        <Stack.Screen name="Plus" component={PlusScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
