import { StackCardInterpolationProps } from '@react-navigation/stack';

export const fade = ({ current }: StackCardInterpolationProps) => {
  return {
    cardStyle: {
      opacity: current.progress.interpolate({
        inputRange: [0, 1],
        outputRange: [0, 1],
        extrapolate: 'clamp',
      }),
    },
  };
};

export const flipToScreen = ({
  current,
  next,
}: StackCardInterpolationProps) => {
  return {
    cardStyle: {
      opacity: current.progress.interpolate({
        inputRange: [0, 0.499, 0.5, 1],
        outputRange: [0, 0, 1, 1],
        extrapolate: 'clamp',
      }),
      transform: [
        // Translation for the animation of the current card
        { perspective: 1000 },
        {
          rotateX: current.progress.interpolate({
            inputRange: [0, 0.5, 0.501, 1],
            outputRange: ['360deg', '270deg', '90deg', '0deg'],
            extrapolate: 'clamp',
          }),
        },
        {
          rotateX: next
            ? next.progress.interpolate({
                inputRange: [0, 0.5, 0.501, 1],
                outputRange: ['360deg', '270deg', '90deg', '0deg'],
                extrapolate: 'clamp',
              })
            : '0deg',
        },
      ],
    },
  };
};
