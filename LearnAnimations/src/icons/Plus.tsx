import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

export const PlusSVG = props => (
  <Svg viewBox="0 0 30 30">
    <Path stroke={'white'} strokeWidth={1} d="M15 5v20M5 15h20" />
  </Svg>
);
