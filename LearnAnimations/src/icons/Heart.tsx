import * as React from 'react';
import Svg, {Path} from 'react-native-svg';
export const HeartSVG = props => (
  <Svg viewBox="0 0 30 30" {...props}>
    <Path
      d="M26.8 17.81 15.59 29.03 3.37 16.81C-4.75 8.69 6.46-2.52 14.58 5.59c8.01-8 20.23 4.22 12.22 12.22z"
      fill="red"
    />
  </Svg>
);
