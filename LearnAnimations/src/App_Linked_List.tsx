import {View, StyleSheet, TouchableOpacity, Animated, Text} from 'react-native';
import React, {memo, useRef, useState} from 'react';
import {PlusSVG} from './icons/Plus';
const Heart: React.FC<{rh: (id: number) => void; id: number}> = memo(
  ({rh, id}) => {
    // console.log('added');
    const position = useRef(new Animated.ValueXY()).current;
    Animated.timing(position, {
      toValue: {x: 100, y: 500},
      duration: 1000,
      useNativeDriver: false,
    }).start(({finished}) => {
      if (finished) {
        console.log('finished', id);
        // rh(id);
      }
    });

    return (
      <Animated.View
        style={[
          styles.heart,
          {
            transform: [{translateY: position.y}, {translateX: position.x}],
          },
          {position: 'absolute'},
        ]}>
        <Text style={{color: 'white'}}>{id}</Text>
      </Animated.View>
    );
  },
);
export default () => {
  const addHeart = () => {};
  const removeHeart = (id: number) => {};

  return (
    <View style={styles.container}>
      <View style={[styles.heartsContainer]}>{heartArray}</View>
      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          addHeart();
        }}>
        <PlusSVG />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'pink',
  },
  heartsContainer: {
    bottom: 30,
    backgroundColor: 'red',
    width: '100%',
    height: '100%',
    // borderWidth: 1,
    // borderColor: 'red',
  },
  heart: {
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'blue',
  },
  button: {
    width: 50,
    height: 50,
    backgroundColor: 'purple',
    position: 'absolute',
    bottom: 60,
    left: 30,
    borderRadius: 25,
  },
});
