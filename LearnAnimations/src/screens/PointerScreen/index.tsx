import React from 'react';
import { StyleSheet, View } from 'react-native';
import { PopUpMenu } from '@components/animated/PopUpMenu';
import { Duck } from '@components/animated/Duck';

export const PointerScreen = ({ navigation }) => {
  return (
    <View style={[styles.container]}>
      {/* <Pointer /> */}
      <Duck />

      <PopUpMenu navigation={navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'cyan',
  },
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: 50,
    height: 50,
    backgroundColor: 'purple',
    position: 'absolute',
    top: 60,
    left: 30,
    borderRadius: 25,
  },
});
