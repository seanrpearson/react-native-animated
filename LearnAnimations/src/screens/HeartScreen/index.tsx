import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { FloatingSVGSpawner } from '@components/animated/FloatingSVGSpawner';
import { PopUpMenu } from '@components/animated/PopUpMenu';
import { HeartSVG } from '@icons/Heart';

export const HeartScreen = ({ navigation }) => {
  const { addHeart, hearts } = FloatingSVGSpawner(<HeartSVG />);
  return (
    <View style={[styles.container]}>
      {hearts}
      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          for (let i = 0; i < 1; i++) {
            addHeart();
          }
        }}>
        <Text style={{ color: 'white', fontSize: 16 }}>{hearts.length}</Text>
      </TouchableOpacity>

      <PopUpMenu navigation={navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'pink',
  },
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: 50,
    height: 50,
    backgroundColor: 'purple',
    position: 'absolute',
    top: 60,
    left: 30,
    borderRadius: 25,
  },
});
