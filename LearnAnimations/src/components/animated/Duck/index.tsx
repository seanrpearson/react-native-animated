import { DuckSVG } from '@icons/Duck';
import React, { useRef, useState } from 'react';
import { Animated, StyleSheet, View } from 'react-native';
import { Gesture, GestureDetector } from 'react-native-gesture-handler';
import { Follower } from '../Follower';

export const Duck = () => {
  let [initalized] = useState(false);
  const [layout, setLayout] = useState({
    y: 0,
    width: 80,
    height: 80,
    x: 0,
  });

  const ptrSize = 40;
  const [translate] = useState(new Animated.ValueXY({ x: 0, y: 0 }));
  const [zDir, setZDir] = useState(0);
  const [xDir, setXDir] = useState(false);

  const gesture = Gesture.Manual().onTouchesMove(event => {
    const xOffset =
      event.changedTouches[0].absoluteX - layout.x - layout.width / 2;
    const yOffset =
      event.changedTouches[0].absoluteY - layout.y - layout.height / 2;
    const x = layout.x + translate.x._value - event.changedTouches[0].x;
    const y = layout.y + translate.y._value - event.changedTouches[0].y;

    const angle = (Math.atan2(-x, -y) * 180) / Math.PI - 90;
    setZDir(angle);
    setXDir(Math.abs(angle) > 90);
    Animated.spring(translate, {
      tension: 1,
      toValue: { x: xOffset, y: yOffset },
      useNativeDriver: false,
    }).start(({ finished }) => {
      if (finished) {
      }
    });
  });

  const animationStyle = {
    transform: [
      { translateX: translate.x },
      { translateY: translate.y },
      { rotateY: '180deg' },
      {
        rotateZ: `${zDir}deg`,
      },
      { rotateX: xDir ? '180deg' : '0deg' },
    ],
  };
  return (
    <GestureDetector gesture={gesture}>
      <View style={styles.container}>
        <Animated.View
          onLayout={event => {
            if (!initalized) {
              initalized = true;
              setLayout(event.nativeEvent.layout);
            }
          }}
          style={[animationStyle]}>
          <DuckSVG height={ptrSize} width={ptrSize} />
        </Animated.View>
        <Follower
          x={layout.x + translate.x._value}
          y={layout.y + translate.y._value}
          left={3}
        />
      </View>
    </GestureDetector>
  );
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 1000,
  },
});
