import { DuckSVG } from '@icons/Duck';
import React, { useEffect, useRef, useState } from 'react';
import { Animated, Dimensions, StyleSheet, View } from 'react-native';
import { Gesture, GestureDetector } from 'react-native-gesture-handler';

export const Pointer = () => {
  const [layout, setLayout] = useState({
    y: 0,
    width: 80,
    height: 80,
    x: 0,
  });

  const [dimensions, setDimensions] = useState(Dimensions.get('window'));
  const ptrSize = 40;
  const translate = useRef(new Animated.ValueXY({ x: 0, y: 0 })).current;
  const [zDir, setZDir] = useState(0);
  const [xDir, setXDir] = useState(false);
  const gesture = Gesture.Manual()
    .onTouchesDown(event => {
      const xOffset =
        event.changedTouches[0].absoluteX - layout.x - layout.width / 2;
      const yOffset =
        event.changedTouches[0].absoluteY - layout.y - layout.height / 2;
      const x = layout.x + translate.x['_value'] - event.changedTouches[0].x;
      const y = layout.y + translate.y['_value'] - event.changedTouches[0].y;
      const angle = (Math.atan2(-x, -y) * 180) / Math.PI - 90;
      setZDir(angle);
      setXDir(Math.abs(angle) > 90);
      Animated.spring(translate, {
        tension: 1,
        toValue: { x: xOffset, y: yOffset },
        useNativeDriver: false,
      }).start();
    })
    .onTouchesMove(event => {
      const xOffset =
        event.changedTouches[0].absoluteX - layout.x - layout.width / 2;
      const yOffset =
        event.changedTouches[0].absoluteY - layout.y - layout.height / 2;
      const x = layout.x + translate.x['_value'] - event.changedTouches[0].x;
      const y = layout.y + translate.y['_value'] - event.changedTouches[0].y;
      const angle = (Math.atan2(-x, -y) * 180) / Math.PI - 90;
      setZDir(angle);
      setXDir(Math.abs(angle) > 90);
      Animated.spring(translate, {
        tension: 1,
        toValue: { x: xOffset, y: yOffset },
        useNativeDriver: false,
      }).start();
    });
  const rStyle = {
    transform: [
      { translateX: translate.x },
      { translateY: translate.y },
      { rotateY: '180deg' },
      {
        rotateZ: `${zDir}deg`,
      },
      { rotateX: xDir ? '180deg' : '0deg' },
    ],
  };
  return (
    <GestureDetector gesture={gesture}>
      <View style={styles.container}>
        {/* <GestureDetector gesture={gesture}> */}
        <Animated.View
          onLayout={event => {
            const layout = event.nativeEvent.layout;
            setLayout(layout);
          }}
          style={[styles.circle, rStyle]}>
          <DuckSVG height={ptrSize} width={ptrSize} />
        </Animated.View>
        {/* </GestureDetector> */}
      </View>
    </GestureDetector>
  );
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    alignItems: 'center',
  },
  circle: {
    backgroundColor: 'blue',
  },
});
