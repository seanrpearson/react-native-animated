import { FloatingSvg } from '../FloatingSvg';
import React, { useState } from 'react';
export const FloatingSVGSpawner = svg => {
  const [hearts, setHearts] = useState<{ [key: string]: Element }>({});
  //removeHeart
  const removeHeart = (key: any) => {
    setHearts(prevState => {
      const copy = Object.assign({}, prevState);
      delete copy[key];
      return copy;
    });
  };

  //addHeart
  const addHeart = () => {
    setHearts(prevState => {
      let key = Date.now();
      console.log(key);
      while (prevState[key]) {
        key = Date.now();
      }
      const copy = Object.assign({}, prevState);
      copy[key] = (
        <FloatingSvg
          svg={svg}
          key={key}
          callback={() => {
            removeHeart(key);
          }}
        />
      );
      return copy;
    });
  };

  return {
    hearts: Object.values(hearts),
    addHeart,
  };
};
