import {
  StyleSheet,
  Animated,
  View,
  TouchableWithoutFeedback,
  Easing,
  Text,
} from 'react-native';
import React, { useRef, useState } from 'react';
import { Button } from '@components/neumorphism/Button';

export const PopUpMenu: React.FC<{ navigation: any }> = ({ navigation }) => {
  const [open, setOpen] = useState(false);
  const toggleOpen = () => {
    setOpen(!open);
    Animated.timing(animation, {
      toValue: Number(!open),
      duration: 200,
      useNativeDriver: false,
    }).start();
  };

  const animation = useRef(new Animated.Value(0)).current;

  return (
    <>
      <TouchableWithoutFeedback onPress={toggleOpen}>
        <Animated.View
          style={[
            styles.button,
            styles.backgroundStyle,
            {
              transform: [
                {
                  scale: animation.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0, 30],
                  }),
                },
              ],
            },
          ]}
        />
      </TouchableWithoutFeedback>
      {['Plus', 'Heart', 'Duck'].map((item, idx) => {
        return (
          <TouchableWithoutFeedback
            key={item}
            onPress={() => {
              toggleOpen();
              navigation.navigate(item);
            }}>
            <Animated.View
              style={[
                styles.button,
                { bottom: 30, backgroundColor: 'green' },

                {
                  transform: [
                    { scale: animation },
                    {
                      translateY: animation.interpolate({
                        inputRange: [0, 0.3, 1],
                        outputRange: [0, -70 * (idx + 1), -70 * (idx + 1)],
                        easing: Easing.inOut(Easing.linear),
                      }),
                    },
                  ],
                },
              ]}>
              <Text>{item}</Text>
            </Animated.View>
          </TouchableWithoutFeedback>
        );
      })}
      <Button onPress={toggleOpen} style={styles.mainMenuButton} />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroundStyle: {
    backgroundColor: 'gray',
    opacity: 0.5,
  },
  mainMenuButton: {
    position: 'absolute',
    bottom: 30,
    right: 30,
    width: 60,
    height: 60,
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 60,
    height: 60,
    backgroundColor: 'red',
    position: 'absolute',
    bottom: 30,
    right: 30,
    borderRadius: 30,
  },
});
