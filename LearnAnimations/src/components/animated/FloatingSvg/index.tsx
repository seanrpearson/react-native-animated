import { StyleSheet, Animated, Dimensions, Easing } from 'react-native';
import React, { useRef } from 'react';

export const FloatingSvg: React.FC<{ svg: any; callback: () => void }> = ({
  svg,
  callback,
}) => {
  const sz = Math.random() * 40 + 10;
  const windowWidth = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;
  const randomXPos = () => {
    return Math.random() * windowWidth - 25;
  };
  const mapRange = (
    value: number,
    a: number,
    b: number,
    c: number,
    d: number,
  ) => {
    // first map value from (a..b) to (0..1)
    value = (value - a) / (b - a);
    // then map it from (0..1) to (c..d) and return it
    return c + value * (d - c);
  };
  const direction = Math.random() - 0.5 < 0 ? -1 : 1;
  const position = useRef(new Animated.Value(0)).current;
  Animated.timing(position, {
    toValue: 1,
    duration: mapRange(sz, 10, 50, 7000, 5000),
    useNativeDriver: false,
  }).start(({ finished }) => {
    if (finished) {
      console.log('finished');
      callback();
    }
  });
  const startXPos = randomXPos();
  const moveXPos = sz;

  const stopPointY = windowHeight / 2;
  const animatedPositionStyle = {
    opacity: position.interpolate({
      inputRange: [0, 1],
      outputRange: [1, 0],
    }),
    transform: [
      {
        translateY: position.interpolate({
          inputRange: [0, 1 / 4, 2 / 4, 3 / 4, 1],
          outputRange: [
            windowHeight - sz,
            windowHeight - (stopPointY * 1) / 4 - sz,
            windowHeight - (stopPointY * 2) / 4 - sz,
            windowHeight - (stopPointY * 3) / 4 - sz,
            windowHeight - stopPointY,
          ],
          easing: Easing.inOut(Easing.linear),
        }),
      },
      {
        translateX: position.interpolate({
          inputRange: [0, 1 / 4, 2 / 4, 3 / 4, 1],
          outputRange: [
            startXPos,
            startXPos + moveXPos * direction,
            startXPos + moveXPos * -direction,
            startXPos + moveXPos * direction,
            startXPos + moveXPos * -direction,
          ],
          easing: Easing.inOut(Easing.linear),
        }),
      },
    ],
  };
  const rotateAmount = 10;
  const animtedRotationStyle = {
    transform: [
      {
        scale: position.interpolate({
          inputRange: [0, 0.1, 0.2, 1],
          outputRange: [0, 1.2, 1, 1],
        }),
      },
      {
        rotate: position.interpolate({
          inputRange: [0, 1 / 4, 2 / 4, 3 / 4, 1],
          outputRange: [
            '0deg',
            `${-direction * rotateAmount}deg`,
            `${direction * rotateAmount}deg`,
            `${-direction * rotateAmount}deg`,
            `${direction * rotateAmount}deg`,
          ],
        }),
      },
    ],
  };
  return (
    <Animated.View
      style={[styles.heart, animatedPositionStyle, { width: sz, height: sz }]}>
      <Animated.View style={[styles.innerHeart, animtedRotationStyle]}>
        {svg}
      </Animated.View>
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  heart: {
    width: 100,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'invisible',
    position: 'absolute',
  },
  innerHeart: { width: '100%', height: '100%' },
});
