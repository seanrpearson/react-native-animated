import { DuckSVG } from '@icons/Duck';
import React, { useEffect, useRef, useState } from 'react';
import { Animated, Dimensions, StyleSheet, View } from 'react-native';
import { Follower as FollowerCopy } from '@components/animated/Follower';

export const Follower = (pointToFollow: {
  x: number;
  y: number;
  left: number;
}) => {
  const [layout, setLayout] = useState({
    y: 0,
    width: 80,
    height: 80,
    x: 0,
  });

  const ptrSize = 20;
  const translate = useRef(new Animated.ValueXY({ x: 0, y: 0 })).current;
  const [zDir, setZDir] = useState(0);
  const [xDir, setXDir] = useState(false);
  useEffect(() => {
    if (pointToFollow.left === 3) {
      console.log(pointToFollow);
    }
    const xOffset = pointToFollow.x - layout.x - layout.width / 2;
    const yOffset = pointToFollow.y - layout.y - layout.height / 2;
    const x = layout.x + translate.x['_value'] - pointToFollow.x;
    const y = layout.y + translate.y['_value'] - pointToFollow.y;
    const angle = (Math.atan2(-x, -y) * 180) / Math.PI - 90;
    setZDir(angle);
    setXDir(Math.abs(angle) > 90);
    Animated.spring(translate, {
      tension: 10,
      toValue: { x: xOffset, y: yOffset },
      useNativeDriver: false,
    }).start();
  }, [pointToFollow, layout, translate]);

  const rStyle = {
    transform: [
      { translateX: translate.x },
      { translateY: translate.y },
      { rotateY: '180deg' },
      {
        rotateZ: `${zDir}deg`,
      },
      { rotateX: xDir ? '180deg' : '0deg' },
    ],
  };
  return (
    <View style={styles.container}>
      <Animated.View
        onLayout={event => {
          const layout = event.nativeEvent.layout;
          setLayout(layout);
        }}
        style={[styles.circle, rStyle]}>
        <DuckSVG height={ptrSize} width={ptrSize} />
      </Animated.View>
      {pointToFollow.left > 0 && (
        <Follower
          x={layout.x + translate.x['_value']}
          y={layout.y + translate.y['_value']}
          left={pointToFollow.left - 1}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    alignItems: 'center',
  },
  circle: {
    // backgroundColor: 'blue',
  },
});
