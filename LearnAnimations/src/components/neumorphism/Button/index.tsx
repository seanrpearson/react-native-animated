import React, { useRef } from 'react';
import { Animated, Pressable, PressableProps, StyleSheet } from 'react-native';

export const Button = ({ style, children, ...rest }: PressableProps) => {
  const animation = useRef(new Animated.Value(1)).current;

  const pressStyle = {
    transform: [{ scale: animation }],
  };

  return (
    <Animated.View style={[styles.button, style, pressStyle]}>
      <Pressable
        onPressIn={() => {
          Animated.timing(animation, {
            toValue: 0.9,
            duration: 200,
            useNativeDriver: false,
          }).start();
        }}
        onPressOut={() => {
          Animated.timing(animation, {
            toValue: 1,
            duration: 200,
            useNativeDriver: false,
          }).start();
        }}
        style={{ flex: 1 }}
        {...rest}>
        {children}
      </Pressable>
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  button: {
    width: 50,
    height: 50,
    borderRadius: 10,
    backgroundColor: '#e0e0e0',
    boxShadow: '20 20 70 #bebebe, -20 -20 70 #ffffff',
    elevation: 5,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 1,
  },
});
